import { Component, OnInit } from '@angular/core';
import { ApiService } from './services/api.service';
import { faTrash } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit{
  constructor(private apiService: ApiService){}
  hits = [];
  faTrash = faTrash;

  ngOnInit(){
    this.apiService.get('/hit')
      .subscribe(
        data => this.setHits(data),
        err => console.log(err)
      );
  }

  setHits(hits) {
    this.hits = this.sortHits(hits);
    console.log(this.hits);
  }

  sortHits(hits) {
    let sort = hits.sort((a, b) => new Date(b.created_at).getTime() - new Date(a.created_at).getTime());
    return sort;
  }

  delete(hit) {
    this.apiService.delete(`/hit/${hit._id}`)
      .subscribe(
        data => this.ngOnInit(),
        err => console.log(err)
      );
  }

  goToLink(hit){
    const url = hit.url ? hit.url : hit.story_url;
    window.open(url, "_blank");
  }
}
