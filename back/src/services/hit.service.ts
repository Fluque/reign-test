import { HttpService, Injectable } from '@nestjs/common';
import { map } from 'rxjs/operators';

@Injectable()
export class HitHttpService {
  constructor(private http: HttpService) {}

  getHits = () =>
    this.http
      .get('https://hn.algolia.com/api/v1/search_by_date?query=nodejs')
      .pipe(map((response) => response.data['hits']));
}
