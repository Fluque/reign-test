import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';

import { HitHttpService } from '../services/hit.service';
import { Hit, HitDocument } from './entities/hit.entity';

@Injectable()
export class HitService {
  constructor(
    @InjectModel('Hit') private hitModel: Model<HitDocument>,
    private hitHttpService: HitHttpService,
  ) {}

  findAll = (): Promise<Hit[]> => this.hitModel.find().exec();

  // findOne(id: number) {
  //   return `This action returns a #${id} hit`;
  // }

  remove = (id: string) => this.hitModel.deleteOne({ _id: id });

  async seedDb() {
    const hits = await this.hitHttpService.getHits().toPromise();
    return await this.saveMany(hits);
  }

  saveMany = (hits: [Hit]) => this.hitModel.insertMany(hits);
}
