import { Controller, Get, Param, Delete } from '@nestjs/common';
import { HitService } from './hit.service';

@Controller('hit')
export class HitController {
  constructor(private readonly hitService: HitService) {}

  @Get()
  findAll() {
    return this.hitService.findAll();
  }

  // @Get(':id')
  // findOne(@Param('id') id: string) {
  //   return this.hitService.findOne(+id);
  // }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.hitService.remove(id);
  }
}
