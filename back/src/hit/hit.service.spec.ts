import { MongooseModule } from '@nestjs/mongoose';
import { Test, TestingModule } from '@nestjs/testing';
import { HitHttpService } from '../services/hit.service';
import { rootMongooseTestModule } from '../test-utils/mongo/MongooseTestModule';
import { HitSchema } from './entities/hit.entity';
import { HitService } from './hit.service';

describe('HitService', () => {
  let service: HitService;
  const hitHttpService = { getHits: () => ['test'] };

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [
        rootMongooseTestModule(),
        MongooseModule.forFeature([{ name: 'Hit', schema: HitSchema }]),
      ],
      providers: [
        HitService,
        {
          provide: HitHttpService,
          useValue: hitHttpService,
        },
      ],
    }).compile();

    service = module.get<HitService>(HitService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
