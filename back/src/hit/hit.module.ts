import { HttpModule, Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { APP_FILTER } from '@nestjs/core';

import { HitHttpService } from 'src/services/hit.service';
import { ExceptionsFilter } from 'src/common/exceptions.filter';

import { HitSchema } from './entities/hit.entity';
import { HitService } from './hit.service';
import { HitController } from './hit.controller';
import { HitSeed } from './seeds/hit.seed';

@Module({
  imports: [
    HttpModule,
    MongooseModule.forFeature([{ name: 'Hit', schema: HitSchema }]),
  ],
  controllers: [HitController],
  providers: [
    HitService,
    HitHttpService,
    HitSeed,
    {
      provide: APP_FILTER,
      useClass: ExceptionsFilter,
    },
  ],
  exports: [HitService],
})
export class HitModule {}
