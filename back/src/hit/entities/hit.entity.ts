import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';
// import * as mongoosePaginate from 'mongoose-paginate-v2';

export type HitDocument = Hit & Document;

@Schema()
export class Hit {
  @Prop()
  title: string;

  @Prop()
  story_title: string;

  @Prop({ required: true })
  author: string;

  @Prop()
  url: string;

  @Prop()
  story_url: string;

  @Prop({ default: Date.now })
  created_at: Date;
}

export const HitSchema = SchemaFactory.createForClass(Hit);
// CharacterSchema.plugin(mongoosePaginate);
