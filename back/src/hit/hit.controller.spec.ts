import { MongooseModule } from '@nestjs/mongoose';
import { Test, TestingModule } from '@nestjs/testing';
import { HitHttpService } from '../services/hit.service';
import {
  rootMongooseTestModule,
  closeInMongodConnection,
} from '../test-utils/mongo/MongooseTestModule';
import { Hit, HitSchema } from './entities/hit.entity';
import { HitController } from './hit.controller';
import { HitService } from './hit.service';
import { HitSeed } from './seeds/hit.seed';

describe('HitController', () => {
  let controller: HitController;
  let hitService: HitService;
  const hitHttpService = { getHits: () => ['test'] };

  const hit = new Hit();
  hit.author = 'test';
  hit.story_title = 'test';
  hit.title = 'test';
  hit.url = 'test';
  hit.story_url = 'test';

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [
        rootMongooseTestModule(),
        MongooseModule.forFeature([{ name: 'Hit', schema: HitSchema }]),
      ],
      controllers: [HitController],
      providers: [
        HitService,
        HitSeed,
        {
          provide: HitHttpService,
          useValue: hitHttpService,
        },
      ],
      exports: [HitService],
    }).compile();

    hitService = module.get<HitService>(HitService);
    controller = module.get<HitController>(HitController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });

  it('findAll', async () => {
    const result = [hit];
    jest
      .spyOn(hitService, 'findAll')
      .mockImplementation(() => Promise.resolve(result));
    expect(await controller.findAll()).toBe(result);
  });

  afterAll(async () => {
    await closeInMongodConnection();
  });
});
