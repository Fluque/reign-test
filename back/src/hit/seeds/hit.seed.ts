import { Command } from 'nestjs-command';
import { Injectable } from '@nestjs/common';

import { HitService } from '../../hit/hit.service';

@Injectable()
export class HitSeed {
  constructor(private readonly hitService: HitService) {}

  @Command({
    command: 'create:hits',
    describe: 'create hists',
    autoExit: true,
  })
  async create() {
    console.log('Iniciando creación de objetos...');
    console.log('Obteniendo objetos de la API...');
    await this.hitService.seedDb();
    console.log('Documentos guardados');
  }
}
