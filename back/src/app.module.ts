import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { HitModule } from './hit/hit.module';
import { HitSeed } from './hit/seeds/hit.seed';
import { CommandModule } from 'nestjs-command';
import { MongooseModule } from '@nestjs/mongoose';

@Module({
  imports: [
    HitModule,
    MongooseModule.forRoot('mongodb://dbmongo/reign'),
    CommandModule,
  ],
  controllers: [AppController],
  providers: [AppService, HitSeed],
})
export class AppModule {}
