## Para realizar la instalación del proyecto se deben cumplir los siguientes requisitos en la maquina.

* Docker
* git

## Pasos instalación

* Nueva terminal
* Clonar proyecto ```git clone https://gitlab.com/Fluque/reign-test.git```
* cd Project_name 
* ```docker-compose up -d --build```
* ```docker exec -it back /bin/bash```
* ```npx nestjs-command create:hits```
* Abrir navegador y abrir ```http://localhost:4200```


## Instalación sin docker


### requisitos
* Node.js
* Mongodb
* Git


## Instalación

* Nueva terminal
* Clonar proyecto ```git clone https://gitlab.com/Fluque/reign-test.git```
* cd Project_name/back
* ```npm install```
* ```npx nestjs-command create:hits```
* ```npm run start```
* Nueva terminal
* cd path/to/project/front
* ```npm install```
* ```ng serve -o```

## Consideraciones

* Los puertos 3000, 4200 y 27017 deben estar libres.
* El proyecto esta levantado en modo desarrollo.
