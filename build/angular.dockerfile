FROM node:14 as build-step
RUN mkdir /app
WORKDIR /app
COPY front/package.json /app
RUN npm install --silent
COPY front/ /app
RUN npm install -g @angular/cli
EXPOSE 4200
CMD ["ng", "serve", "--host", "0.0.0.0"]