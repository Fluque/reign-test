FROM node:14

ENV PORT 3000
ENV NPM_CONFIG_LOGLEVEL info
ENV NODE_ENV local


ENV MONGODB_URI mongodb://dbmongo:27017
RUN mkdir -p /usr/src/app
WORKDIR /usr/src/app

COPY back/package*.json /usr/src/app/
RUN npm install --silent 
COPY back/ /usr/src/app
EXPOSE 3000
CMD ["npm", "start"]